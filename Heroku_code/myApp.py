# Begin imports
import re
import sys
import os
import base64

import numpy as np

from flask      import Flask, \
                       render_template, \
                       request, \
                       jsonify
from scipy.misc import imread, \
                       imresize
from load       import init
from joblib     import load
# End imports

sys.path.append(os.path.abspath('./model'))

app = Flask(__name__)

# Begin global variables
global model
global graph
regModel = load('regModel.pkl')
# End global variables

# Begin initialize variables
model, graph = init()
# End initialize variables

# Begin decoding an image from base64 into raw representation
def convertImage(imgData):
    imgstr = re.search(r'base64,(.*)', str(imgData)).group(1)

    with open('output.png', 'wb') as output:
        output.write(base64.b64decode(imgstr))
# End decoding an image from base64 into raw representation

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/classification')
def classification():
    return render_template('classification.html')

@app.route('/regression')
def regression():
    return render_template('regression.html')


@app.route('/predict/', methods=['GET', 'POST'])
def predict():
    imgData = request.get_data()
    convertImage(imgData)
    x = imread('output.png', mode='L')
    x = imresize(x, (28, 28))
    x = x.reshape(1, 28, 28, 1)

    with graph.as_default():
        out = model.predict(x)
        print(out)
        print(np.argmax(out, axis = 1))
        response = np.argmax(out, axis = 1)

        return str(response[0])

@app.route('/regression_predict', methods=['POST'])
def regression_predict():
    feature_array = request.get_json()
    print(feature_array)

    prediction = regModel.predict([feature_array]).tolist()

    response = {}
    response['predictions'] = prediction

    return jsonify(response)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)
