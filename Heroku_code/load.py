import keras

import tensorflow as tf

from keras.models import load_model

def init():
    model = load_model('classification_model.h5')
    print('Loaded model from disk.')

    model.compile(loss=keras.losses.categorical_crossentropy, \
                  optimizer=keras.optimizers.Adadelta(), \
                  metrics=['accuracy'])

    graph = tf.get_default_graph()

    return model, graph
